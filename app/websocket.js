var sockjs = require('sockjs'),
    debounce = require('debounce');

var Websocket = (function() {

  function Websocket(redis) {
    this.redis = redis;
    this.connections = [];
    this.broadcast = debounce(this.broadcast, 200, true);
    this.sockjs = sockjs.createServer({
      log: function(severity, message) {}
    });

    this.initEvents();
  }

  (function(fn) {
    fn.initEvents = function() {
      var self = this;

      self.sockjs.on('connection', function(conn) {
        self.connections.push(conn);

        // Greet our new guest with some data
        self.redis.mget('positive', 'negative', function(err, replies) {
          conn.write(JSON.stringify(replies));
        });

        conn.on('close', function() {
          // Remove the connection from the connections array
          self.connections.forEach(function(elem, i) {
            if(elem == conn) self.connections.splice(i, 1);
          });
        })
      });
    };

    fn.broadcast = function(msg) {
      msg = JSON.stringify(msg);

      this.connections.forEach(function(connection) {
        connection.write(msg);
      });
    };
  })(Websocket.prototype);

  return Websocket;

})();

module.exports = Websocket;
