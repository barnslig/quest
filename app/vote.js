var request = require('request');

var Vote = (function() {

  function Vote(websocket, redis) {
    this.websocket = websocket;
    this.redis = redis;

    this.route = this.route.bind(this);
  }

  (function(fn) {
    fn.route = function(req, res) {
      var self = this,
          vote,
          remote_ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

      // TODO get rid of the callback hell
      self.verifyCaptcha(req.body['g-recaptcha-response'], remote_ip, function(success) {
        if(!success) return res.send('Captcha failed');

        // Save the vote
        vote = req.body.vote == 'yes';
        self.vote(vote, function() {

          // Get current vote count
          self.redis.mget('positive', 'negative', function(err, replies) {
            self.websocket.broadcast(replies);

            // Set a damn cookie to prevent dumbasses from voting multiple times
            res.cookie(process.env.COOKIE_NAME, 'true');
            res.send('Voted!');
          });
        });
      });
    };

    fn.vote = function(vote, cb) {
      var key = vote ? 'positive' : 'negative';
      this.redis.incr(key, cb);
    };

    fn.verifyCaptcha = function(captcha_response, remote_ip, cb) {
      request.post({
        url: 'https://www.google.com/recaptcha/api/siteverify',
        form: {
          secret: process.env.RECAPTCHA_SECRET,
          response: captcha_response,
          remoteip: remote_ip
        }
      }, function(err, res, body) {
        body = JSON.parse(body);
        cb(body.success);
      });
    };
  })(Vote.prototype);

  return Vote;

})();

module.exports = Vote;
