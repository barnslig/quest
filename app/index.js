require('dotenv').load();

var http = require('http'),
    express = require('express'),
    bodyParser = require('body-parser'),
    path = require('path'),
    redis = require('redis'),
    Websocket = require('./websocket'),
    Vote = require('./vote');

var App = (function() {

  function App() {
    this.app = express();
    this.server = http.createServer(this.app);
    this.redis = redis.createClient();

    // Parse freaking POST data
    this.app.use(bodyParser.urlencoded({
      extended: true
    }));
    
    this.setupRoutes();
    this.setupRedis();
  }

  (function(fn) {
    fn.setupRoutes = function() {
      var websocket, vote;

      // Static, precompiled files are served at /
      this.app.use(express.static(path.resolve(__dirname, '../static')));

      // sockjs is served at /ws
      websocket = new Websocket(this.redis);
      websocket.sockjs.installHandlers(this.server, {
        prefix: '/ws'
      });

      // Voter application is served at /vote
      vote = new Vote(websocket, this.redis);
      this.app.post('/vote', vote.route);
    };

    fn.setupRedis = function() {
      this.redis.setnx('positive', '0');
      this.redis.setnx('negative', '0');
    };

    fn.listen = function(host, port) {
      this.server.listen(port, host, function() {
        console.log('App is listening on', host, 'port', port);
      })
    };
  })(App.prototype);

  return App;

})();

module.exports = App;
