var $ = require('jquery'),
    React = require('react'),
    App = require('./components'),
    Statistics = require('./statistics');

$(document).ready(function() {
  new Statistics($('#statistics'));

  React.render(
    <App />,
    $('#app').get(0)
  );
});
