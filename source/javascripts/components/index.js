var React = require('react'),
    Vote = require('./vote');

var App = React.createClass({
  getInitialState: function() {
    return {
      hasVoted: false
    };
  },

  getDefaultProps: function() {
    return {
      cookieRegex: new RegExp(process.env.COOKIE_NAME)
    }
  },

  componentWillMount: function() {
    var hasVoted = document.cookie.match(this.props.cookieRegex) != null;

    this.setState({
      hasVoted: hasVoted
    });
  },

  render: function() {
    if(!this.state.hasVoted) return <Vote />;

    return <span>Du hast bereits abgestimmt!</span>;
  }
});

module.exports = App;
