var React = require('react'),
    ReCAPTCHA = require('react-google-recaptcha');

var Vote = React.createClass({
  getInitialState: function() {
    return {
      showCaptcha: false,
      showSubmit: false
    };
  },

  getDefaultProps: function() {
    return {
      recaptchaKey: process.env.RECAPTCHA_SITEKEY
    };
  },

  handleRadioChange: function(e) {
    this.setState({
      showCaptcha: true
    });
  },

  handleCaptchaChange: function(e) {
    this.setState({
      showSubmit: true
    });
  },

  renderSubmit: function() {
    if(!this.state.showSubmit) return;

    return <button type="submit">Abstimmen</button>;
  },

  renderCaptcha: function() {
    if(!this.state.showCaptcha) return;

    return <ReCAPTCHA sitekey={this.props.recaptchaKey} onChange={this.handleCaptchaChange} />;
  },

  render: function() {
    return (
      <form className="vote" action="/vote" method="POST">
        <label>
          <input type="radio" name="vote" value="yes" onChange={this.handleRadioChange} />
          <div>Ja</div>
        </label>
        <label>
          <input type="radio" name="vote" value="no" onChange={this.handleRadioChange} />
          <div>Nein</div>
        </label>

        {this.renderCaptcha()}
        {this.renderSubmit()}
      </form>
    );
  }
});

module.exports = Vote;
