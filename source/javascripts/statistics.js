var $ = require('jquery'),
    SockJS = require('sockjs-client');

var Statistics = (function() {

  function Statistics(elem) {
    this.elem = elem;
    this.socket = new SockJS('/ws');

    this.setupEvents();
  }

  (function(fn) {
    fn.setupEvents = function() {
      this.socket.onmessage = this.handleMessage.bind(this);
    };

    fn.handleMessage = function(e) {
      var data, all;

      // Fix types
      data = JSON.parse(e.data).map(function(elem) {
        return parseInt(elem);
      });
      all = data[0] + data[1];

      $(this.elem).html('Dafür: ' + Math.floor(data[0]/all*100) + '%. Dagegen: ' + Math.floor(data[1]/all*100) + '%');
    };
  })(Statistics.prototype);

  return Statistics;

})();

module.exports = Statistics;
