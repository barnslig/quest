# quest
Web app that implements a simple yes/no poll with live reporting.

## Setup
1. Copy `.env.example` to `.env` and set appropriate values, e.g. register at [reCAPTCHA](https://www.google.com/recaptcha)
2. Setup and start [Redis](http://redis.io)
3. `npm install`
4. `npm start`
5. Second terminal: `gulp`
6. Visit [localhost:4711](http://localhost:4711)

## Deployment
1. Do not forget to set up your top-secret `.env`-file!
2. Setup and start [Redis](http://redis.io)
3. `npm install` (Yes, without production so we get all the famous gulp packages!)
4. `NODE_ENV=production gulp build`
5. `NODE_ENV=production npm start`
6. Point your nginx/whatever to [localhost:4711](http://localhost:4711) and better configure `/static` to be served through your webserver

## Contributing
The code is licensed within the terms of the MIT license. See LICENSE for a copy.

Please do not forget to add your name and e-mail address to the AUTHORS file!
