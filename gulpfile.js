require('dotenv').load();

var gulp = require('gulp'),
    gulpif = require('gulp-if'),
    path = require('path'),
    del = require('del'),
    gutil = require('gulp-util'),
    livereload = require('gulp-livereload'),
    browserify = require('browserify'),
    watchify = require('watchify'),
    babelify = require('babelify'),
    debowerify = require('debowerify'),
    envify = require('envify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    stylus = require('gulp-stylus'),
    jade = require('gulp-jade');

var config = {
  output: 'static/',
  source: {
    stylesheets: {
      base: 'source/stylesheets/',
      main: 'app.styl',
      watch: '**/*'
    },
    javascripts: {
      base: 'source/javascripts/',
      main: 'app.js',
      watch: '**/*'
    },
    layouts: {
      base: 'source',
      main: '*.jade',
      watch: '*'
    }
  }
};

function do_browserify_build(bundler, is_production_build) {
  bundler.bundle()
    .on('error', gutil.log.bind(gutil, 'Browserify Error'))
    .pipe(source(config.source.javascripts.main))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
      .pipe(gulpif(is_production_build, uglify()))
    .pipe(sourcemaps.write('.'))
    .pipe(livereload())
    .pipe(gulp.dest(config.output));
}

function do_browserify(watch) {
  var bundler = browserify({
    debug: true,
    entries: path.join(config.source.javascripts.base, config.source.javascripts.main),
    cache: {},
    packageCache: {}
  })
    .transform(babelify, {
      compact: false,
      ignore: /(bower_components)|(node_modules)/
    })
    .transform(debowerify)
    .transform(envify);

  if(watch) {
    bundler = watchify(bundler);
    bundler.on('update', function() {
      do_browserify_build(bundler, false);
    });
    bundler.on('log', function(msg) {
      console.log(msg);
    });
  }

  do_browserify_build(bundler, true);
}

gulp.task('javascripts', function() {
  do_browserify(false);
});

gulp.task('watch-javascripts', function() {
  do_browserify(true);
});

gulp.task('stylesheets', function() {
  gulp.src(path.join(config.source.stylesheets.base, config.source.stylesheets.main))
    .pipe(sourcemaps.init())
    .pipe(stylus())
    .pipe(sourcemaps.write('.'))
    .pipe(livereload())
    .pipe(gulp.dest(config.output));
});

gulp.task('watch-stylesheets', ['stylesheets'], function() {
  gulp.watch(path.join(config.source.stylesheets.base, config.source.stylesheets.watch), ['stylesheets']);
});

gulp.task('layouts', function() {
  gulp.src(path.join(config.source.layouts.base, config.source.layouts.main))
    .pipe(jade())
    .pipe(livereload())
    .pipe(gulp.dest(config.output));
});

gulp.task('watch-layouts', ['layouts'], function() {
  gulp.watch(path.join(config.source.layouts.base, config.source.layouts.watch), ['layouts']);
});

gulp.task('clean', function(cb) {
  del([
    path.join(config.output, '**/*'),
    path.join('!', config.output, '.gitkeep')
  ], cb);
});

gulp.task('build', ['clean', 'javascripts', 'stylesheets', 'layouts']);

gulp.task('watch', ['watch-javascripts', 'watch-stylesheets', 'watch-layouts'], function() {
  livereload.listen({port: 35728});
});

gulp.task('default', ['watch']);
